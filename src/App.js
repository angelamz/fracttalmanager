import React,{useState} from 'react';
import './App.css';
import DialogMultiline from 'fracttalcore';

function App() {
  const [openDialog, setOpenDialog] = useState(false);
  return (
    <div className="App">      
      <button onClick={setOpenDialog(true)}>Prueba</button>
      <DialogMultiline
          openDialogConfirm={openDialog}
          handleClose={() => setOpenDialog(false)}
          handleYes={() => {
            setOpenDialog(false);           
          }}
          msg={'PRUEBA SUBMODULOS'}
          title={'INFORMATION'}
          footerTitle={'DO_YOU_WANT_CONTINUE'}
          isMap={false}
       />
    </div>
    
  );
}

export default App;
